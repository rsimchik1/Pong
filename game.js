﻿var fps = 30;
var HEIGHT = 500;
var WIDTH = 800;
var paddleh = 70;
var paddlew = 20;
var paddleonex = 0;
var paddletwox = WIDTH - paddlew;
var paddleoney = (HEIGHT/2) - (paddleh/2);
var paddletwoy = (HEIGHT/2) - (paddleh/2);
var vpaddle = 10;
var x = WIDTH/2;
var y = HEIGHT/2;
var r = 10;
var vx = 10;
var vy = 10;
var scoreone = 0;
var scoretwo = 0;
var maxscore = 7;
var animate;
var resetkey;
var start;

var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

function mainscreen(){
	ctx.fillStyle = '#00ff00';
	ctx.font = '100px helvetica';
	ctx.textAlign = 'center';
	ctx.textBaseline = 'hanging';
	ctx.fillText('PONG', WIDTH/2, HEIGHT/4);
	ctx.font = '30px helvetica';
	ctx.fillText('Press P to play.', WIDTH/2, (3*HEIGHT)/4);
	start = setInterval(function(){
		if (keys[80] == true){play();}
	},0);
}

function reset(){
	paddleonex = 0;
	paddletwox = WIDTH - paddlew;
	paddleoney = (HEIGHT/2) - (paddleh/2);
	paddletwoy = (HEIGHT/2) - (paddleh/2);
	x = WIDTH/2;
	y = HEIGHT/2;
	vx = 10;
	vy = 10;
	scoreone = 0;
	scoretwo = 0;
}

function paddles(){
	ctx.fillStyle = '#00ff00';
	ctx.fillRect(paddleonex,paddleoney,paddlew,paddleh);
	ctx.fillRect(paddletwox,paddletwoy,paddlew,paddleh);
}

function ball(){
	ctx.beginPath();
	ctx.arc(x, y, r, 0, 2*Math.PI);
	ctx.fill();
}

function board(){
	if(x+r>=WIDTH/2 && x-r<=WIDTH/2){
		ctx.lineWidth = 5;
	}else{
		ctx.lineWidth = 1;
	}
	ctx.strokeStyle = '#00ff00';
	ctx.beginPath();
	ctx.moveTo(WIDTH/2, 0);
	ctx.lineTo(WIDTH/2, HEIGHT);
	ctx.stroke();
}

function score(){
	if(x-r>=WIDTH){
		x=WIDTH/2;
		vx=-vx;
		scoreone++;
	}
	if(x+r<=0){
		x=WIDTH/2;
		vx=-vx;
		scoretwo++;
	}
	ctx.lineWidth = 1;
	ctx.textBaseline = 'alphabetic';
	ctx.font = '40px helvetica';
	ctx.textAlign = 'start';
	ctx.strokeText(scoreone, 10, 40);
	ctx.textAlign = 'end';
	ctx.strokeText(scoretwo, WIDTH-10, 40);
}

function end(){
	if(scoreone==maxscore || scoretwo==maxscore){
		window.clearInterval(animate);
		animate = 0;
		ctx.clearRect(0,0,WIDTH,HEIGHT);
		ctx.textAlign = 'center';
		ctx.textBaseline = 'middle';
		ctx.font = '100px helvetica';
		scoreone==maxscore ? ctx.fillText("Player One Wins!", WIDTH/2, HEIGHT/2) : ctx.fillText("Player Two Wins!", WIDTH/2, HEIGHT/2);
		ctx.strokeText(scoreone+' - '+scoretwo, WIDTH/2, (3*HEIGHT)/4);
		ctx.font = '30px helvetica';
		ctx.textBaseline = 'hanging';
		ctx.fillText('Press R to restart.',WIDTH/2, 10);
		resetkey = setInterval(function(){if (keys[82] == true){reset(); play();}},0);
	}
}

function collision(){
	if(y-r<=0 || y+r>=HEIGHT){vy = -vy}
	if(x-r==paddleonex+paddlew){
		if(y+r>=paddleoney && y-r<=paddleoney + paddleh){
			vx = -vx;
		}
	}
	if(x-r==paddletwox-paddlew){
		if(y+r>=paddletwoy && y-r<=paddletwoy + paddleh){
			vx = -vx;
		}
	}
}

var keys = [];

window.addEventListener('keydown',function(e){
	keys[e.keyCode] = true;
},false);
window.addEventListener('keyup',function(e){
	delete keys[e.keyCode];
},false);

function controls() {
	if (keys[38] == true && paddletwoy>0){paddletwoy += -vpaddle}
	if (keys[40] == true && (paddletwoy+paddleh)<HEIGHT){paddletwoy += vpaddle}
	if (keys[87] == true && paddleoney>0){paddleoney += -vpaddle}
	if (keys[83] == true && (paddleoney+paddleh)<HEIGHT){paddleoney += vpaddle}
}

function draw(){
	update();
}

function update(){
	ctx.clearRect(0,0,WIDTH,HEIGHT);
	collision();
	x+=vx;
	y+=vy;
	board();
	controls();
	paddles();
	ball();
	score();
	end();
}

function play(){
	window.clearInterval(resetkey);
	resetkey = 0;
	window.clearInterval(start);
	start = 0;
	animate = setInterval(function(){
		draw();
	}, 1000/fps);
}

mainscreen();